<?php

namespace App\Tests\Command\Parameter;

use App\Repository\ParameterRepository;
use Exception;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class ImportCommandTest extends WebTestCase
{
    /**
     * @dataProvider getFilePaths
     * @throws Exception
     */
    public function testExecute(string $filePath)
    {
        self::bootKernel();
        $application = new Application(self::$kernel);

        $command = $application->find('app:parameter:import');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'file-path' => $filePath,
        ]);

        $output = $commandTester->getDisplay();
        $this->assertStringNotContainsString('ERROR', $output);
        $this->assertStringContainsString('SUCCESS : Parameters imported successfully', $output);

        $parameterRepository = self::getContainer()->get(ParameterRepository::class);
        $parameters = $parameterRepository->findAll();

        $this->assertNotEmpty($parameters);
    }

    public function testExecuteWithInvalidFilePath()
    {
        self::bootKernel();
        $application = new Application(self::$kernel);

        $command = $application->find('app:parameter:import');
        $commandTester = new CommandTester($command);
        $code = $commandTester->execute([
            'file-path' => '../invalid_file_path.example.json',
        ]);

        $this->assertEquals(1, $code);
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('ERROR', $output);
    }

    /**
     * @dataProvider getFilePaths
     */
    public function testExecuteWithOptionIfNotSet(string $filePath)
    {
        self::bootKernel();
        $application = new Application(self::$kernel);

        $command = $application->find('app:parameter:import');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'file-path' => $filePath,
            '--if-not-set' => true,
        ]);

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Parameters are already set. Skipping...', $output);
    }

    public function getFilePaths(): array
    {
        return [
            [__DIR__.'/../../../parameters.example.yaml'],
            [__DIR__.'/../../../parameters.example.json'],
        ];
    }
}