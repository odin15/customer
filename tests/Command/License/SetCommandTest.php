<?php

namespace App\Tests\Command\License;

use App\DataFixtures\LicenseFixtures;
use App\Entity\License;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class SetCommandTest extends WebTestCase
{
    protected ReferenceRepository $referenceRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->referenceRepository = self::getContainer()->get(DatabaseToolCollection::class)->get()->loadFixtures([
            LicenseFixtures::class,
        ])->getReferenceRepository();
    }

    public function testExecute()
    {
        /** @var License $license */
        $license = $this->referenceRepository->getReference(LicenseFixtures::REFERENCE_LICENSE_USER);
        $newValue = $license->getValue() + 5;

        $application = new Application(self::$kernel);

        $command = $application->find('app:license:set');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'type' => License::TYPE_USER,
            'value' => $newValue,
        ]);

        $output = $commandTester->getDisplay();
        $this->assertStringNotContainsString('ERROR', $output);
        $this->assertStringContainsString('SUCCESS : License of type "'.License::TYPE_USER.'" is now "'.$newValue.'"', $output);
    }

    public function testExecuteWithInvalidLicenseType()
    {
        $application = new Application(self::$kernel);

        $command = $application->find('app:license:set');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'type' => 'ThisLicenseDoesNotExist',
            'value' => 1,
        ]);

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('ERROR', $output);
    }

    public function testExecuteWithOptionIfNotSet()
    {
        $type = License::TYPE_USER;
        $application = new Application(self::$kernel);

        $command = $application->find('app:license:set');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'type' => $type,
            'value' => 1,
            '--if-not-set' => true,
        ]);

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('License type "'.$type.'" is already set. Skipping...', $output);
    }
}
