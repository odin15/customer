<?php

namespace App\Tests\GraphQL\Mutation\License;

use App\DataFixtures\LicenseFixtures;
use App\Entity\License;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Odeven\GraphQLTestBundle\GraphQL\GraphQLTestCase;

class UserLicenseMutationTest extends GraphQLTestCase
{
    protected ReferenceRepository $referenceRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->referenceRepository = self::getContainer()->get(DatabaseToolCollection::class)->get()->loadFixtures([
            LicenseFixtures::class,
        ])->getReferenceRepository();
    }

    public function testMutationIncrementUserLicenseWithAvailableLicenses()
    {
        $this->assertQuery('
            mutation incrementUserLicense {
                incrementUserLicense
            }
        ', [
            'data' => [
                'incrementUserLicense' => true,
            ],
        ], [], [
            'X-Service' => 'api-gateway',
        ]);
    }

    public function testMutationIncrementUserLicenseWithoutAvailableLicenses()
    {
        /** @var License $license */
        $license = $this->referenceRepository->getReference(LicenseFixtures::REFERENCE_LICENSE_USER);
        $license->setUsed($license->getValue());
        self::getContainer()->get(EntityManagerInterface::class)->flush();

        $this->assertQuery('
            mutation incrementUserLicense {
                incrementUserLicense
            }
        ', [
            'data' => [
                'incrementUserLicense' => false,
            ],
        ], [], ['X-Service' => 'api-gateway']);
    }

    public function testMutationIncrementUserLicenseWithNonExistingEntity()
    {
        /** @var License $license */
        $license = $this->referenceRepository->getReference(LicenseFixtures::REFERENCE_LICENSE_USER);

        $em = self::getContainer()->get(EntityManagerInterface::class);
        $em->remove($license);
        $em->flush();

        $this->assertQuery('
            mutation incrementUserLicense {
                incrementUserLicense
            }
        ', [
            'data' => [
                'incrementUserLicense' => false,
            ],
        ], [], ['X-Service' => 'api-gateway']);
    }
}
