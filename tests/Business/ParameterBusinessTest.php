<?php

namespace App\Tests\Business;

use App\Business\ParameterBusiness;
use App\DataFixtures\ParameterFixtures;
use App\Entity\Parameter;
use App\Repository\ParameterRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ParameterBusinessTest extends WebTestCase
{
    protected ReferenceRepository $referenceRepository;

    protected ParameterBusiness $parameterBusiness;

    /**
     * @throws Exception
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->referenceRepository = self::getContainer()->get(DatabaseToolCollection::class)->get()->loadFixtures([
            ParameterFixtures::class,
        ])->getReferenceRepository();

        $this->parameterBusiness = self::getContainer()->get(ParameterBusiness::class);
    }

    public function testExists()
    {
        $this->assertTrue($this->parameterBusiness->exists('CUSTOMER_NAME'));
        $this->assertFalse($this->parameterBusiness->exists('THIS_PARAMETER_DOES_NOT_EXISTS'));
    }

    /**
     * @throws Exception
     */
    public function testGetValueWithExistingParameter()
    {
        $value = $this->parameterBusiness->getValue('CUSTOMER_NAME');

        $this->assertNotNull($value);
        $this->assertEquals('Dyosis', $value);
    }

    /**
     * @throws Exception
     */
    public function testGetValueInMemory()
    {
        $this->parameterBusiness->getValue('CUSTOMER_NAME');
        $value = $this->parameterBusiness->getValue('CUSTOMER_NAME');

        $this->assertNotNull($value);
        $this->assertEquals('Dyosis', $value);
    }

    /**
     * @throws Exception
     */
    public function testGetValueWithNonExistingParameter()
    {
        $value = $this->parameterBusiness->getValue('BOOKEO_KEYS');

        $this->assertEmpty($value);
    }

    public function testGetValueWithInvalidParameter()
    {
        $this->expectException(Exception::class);
        $this->parameterBusiness->getValue('THIS_PARAMETER_DOES_NOT_EXISTS');
    }

    /**
     * @throws Exception
     */
    public function testSetValueWithExistingParameter()
    {
        $name = 'CUSTOMER_NAME';
        $newValue = 'Odin';
        $em = self::getContainer()->get(EntityManagerInterface::class);
        $this->parameterBusiness->setValue($name, $newValue);
        $em->flush();

        $value = $this->parameterBusiness->getValue($name);

        $this->assertNotNull($value);
        $this->assertEquals($newValue, $value);

        $parameterRepository = self::getContainer()->get(ParameterRepository::class);
        $parameter = $parameterRepository->findOneByName($name);

        $this->assertNotNull($parameter);
        $this->assertEquals($newValue, $parameter->getValue());
    }

    /**
     * @throws Exception
     */
    public function testSetValueInMemory()
    {
        $value = $this->parameterBusiness->getValue('CUSTOMER_NAME');
        $this->assertEquals('Dyosis', $value);

        $this->parameterBusiness->setValue('CUSTOMER_NAME', 'test');
        $value = $this->parameterBusiness->getValue('CUSTOMER_NAME');
        $this->assertEquals('test', $value);
    }

    public function testSetValueWithInvalidParameter()
    {
        $this->expectException(Exception::class);
        $this->parameterBusiness->setValue('THIS_PARAMETER_DOES_NOT_EXISTS', 'test');
    }

    /**
     * @throws Exception
     */
    public function testGetParameterValue()
    {
        $this->assertEquals('test', $this->parameterBusiness->getParameterValue((new Parameter())
            ->setName('TEST')
            ->setType('string')
            ->setValue('test')
        ));
        $this->assertEquals(1, $this->parameterBusiness->getParameterValue((new Parameter())
            ->setName('TEST')
            ->setType('int')
            ->setValue('1')
        ));
        $this->assertEquals(1.2, $this->parameterBusiness->getParameterValue((new Parameter())
            ->setName('TEST')
            ->setType('float')
            ->setValue('1.2')
        ));
        $this->assertInstanceOf(DateTimeInterface::class, $this->parameterBusiness->getParameterValue((new Parameter())
            ->setName('TEST')
            ->setType('date')
            ->setValue('2020-12-31')
        ));
        $this->assertInstanceOf(DateTimeInterface::class, $this->parameterBusiness->getParameterValue((new Parameter())
            ->setName('TEST')
            ->setType('datetime')
            ->setValue('2020-12-31 12:30:00')
        ));
        $this->assertIsArray($this->parameterBusiness->getParameterValue((new Parameter())
            ->setName('TEST')
            ->setType('json')
            ->setValue('{"test": "test"}')
        ));

        $this->expectException(Exception::class);
        $this->parameterBusiness->getParameterValue((new Parameter())
            ->setName('TEST')
            ->setType('invalid type')
        );
    }

    public function testSetParameterValue()
    {
        $parameter = (new Parameter())
            ->setName('TEST')
            ->setType('string')
        ;
        $this->parameterBusiness->setParameterValue($parameter, 'test');
        $this->assertEquals('test', $parameter->getValue());

        $parameter = (new Parameter())
            ->setName('TEST')
            ->setType('date')
        ;
        $this->parameterBusiness->setParameterValue($parameter, new DateTime('2020-12-31'));
        $this->assertStringStartsWith('2020-12-31', $parameter->getValue());

        $parameter = (new Parameter())
            ->setName('TEST')
            ->setType('datetime')
        ;
        $this->parameterBusiness->setParameterValue($parameter, new DateTime('2020-12-31 12:30:00'));
        $this->assertStringStartsWith('2020-12-31T12:30:00', $parameter->getValue());

        $parameter = (new Parameter())
            ->setName('TEST')
            ->setType('json')
        ;
        $this->parameterBusiness->setParameterValue($parameter, [
            'test' => 'test',
        ]);
        $this->assertEquals('{"test":"test"}', $parameter->getValue());
    }

    /**
     * @throws Exception
     */
    public function testGetParameter()
    {
        $parameter = $this->parameterBusiness->getParameter('CUSTOMER_NAME');

        $this->assertNotNull($parameter);
    }

    /**
     * @throws Exception
     */
    public function testGetParameterWithNonExistingParameter()
    {
        $parameter = $this->parameterBusiness->getParameter('CUSTOMER_NAME');

        $this->assertNotNull($parameter);
    }

    public function testGetParameterWithInvalidParameter()
    {
        $this->expectException(Exception::class);
        $this->parameterBusiness->getParameter('THIS_PARAMETER_DOES_NOT_EXISTS');
    }

    /**
     * @throws Exception
     */
    public function testGetAllParameters()
    {
        $parameters = $this->parameterBusiness->getAllParameters();

        $this->assertGreaterThanOrEqual(2, $parameters);
    }
}