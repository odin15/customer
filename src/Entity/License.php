<?php

namespace App\Entity;

use App\Repository\LicenseRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: LicenseRepository::class)]
class License
{
    const TYPE_USER = 'USER';

    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue]
    private int $id;

    #[ORM\Column(type: 'string', length: 4)]
    private string $type;

    #[ORM\Column(type: 'integer', length: 3)]
    private int $value;

    #[ORM\Column(type: 'integer', length: 3)]
    private int $used = 0;

    public function getId(): int
    {
        return $this->id;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;
        return $this;
    }

    public function getUsed(): int
    {
        return $this->used;
    }

    public function setUsed(int $used): self
    {
        $this->used = $used;
        return $this;
    }

    public static function getValidTypes(): array
    {
        return [
            self::TYPE_USER,
        ];
    }
}
