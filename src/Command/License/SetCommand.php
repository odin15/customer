<?php

namespace App\Command\License;

use App\Entity\License;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Exception;
use Symfony\Contracts\Service\Attribute\Required;

#[AsCommand(name: 'app:license:set', description: 'Set a license')]
class SetCommand extends Command
{
    protected ValidatorInterface $validator;

    protected EntityManagerInterface $entityManager;

    protected MessageBusInterface $bus;

    #[Required]
    public function setValidator(ValidatorInterface $validator): self
    {
        $this->validator = $validator;
        return $this;
    }

    #[Required]
    public function setEntityManager(EntityManagerInterface $entityManager): self
    {
        $this->entityManager = $entityManager;
        return $this;
    }

    #[Required]
    public function setBus(MessageBusInterface $bus): self
    {
        $this->bus = $bus;
        return $this;
    }

    protected function configure()
    {
        $this
            ->addArgument('type', InputArgument::REQUIRED, 'Type of license')
            ->addArgument('value', InputArgument::REQUIRED, 'Value of the license')
            ->addOption('if-not-set', null, InputOption::VALUE_OPTIONAL, 'Only set if no value exists', false);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $type = $input->getArgument('type');
            $value = (int)$input->getArgument('value');
            $ifNotSet = $input->getOption('if-not-set');

            $licenseRepository = $this->entityManager->getRepository(License::class);

            $license = $licenseRepository->findOneBy([
                'type' => $type,
            ]);
            if (null === $license) {
                $license = new License();
                $license
                    ->setType($type);
            } else if (false !== $ifNotSet) {
                $output->writeln('License type "' . $type . '" is already set. Skipping...');

                return 0;
            }

            $license->setValue($value);

            $errors = $this->validator->validate($license);
            if (count($errors)) {
                $messages = [];
                foreach ($errors as $error) {
                    $messages[] = $error->getMessage();
                }
                throw new Exception(implode("\r\n\t- ", $messages));
            }

            $this->entityManager->persist($license);
            $this->entityManager->flush();

            $output->writeln('SUCCESS : License of type "' . $type . '" is now "' . $license->getValue() . '"');

            return Command::SUCCESS;
        } catch (Exception $exception) {
            $output->writeln('ERROR : ' . $exception->getMessage());

            return Command::FAILURE;
        }
    }
}
