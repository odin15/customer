<?php

namespace App\Command\Parameter;

use App\Business\ParameterBusiness;
use App\Entity\Parameter;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;
use Symfony\Contracts\Service\Attribute\Required;

#[AsCommand(name: 'app:parameter:import', description: 'Import parameters from a file')]
class ImportCommand extends Command
{
    protected ParameterBusiness $parameterBusiness;

    protected EntityManagerInterface $entityManager;

    #[Required]
    public function setParameterBusiness(ParameterBusiness $parameterBusiness): self
    {
        $this->parameterBusiness = $parameterBusiness;

        return $this;
    }

    #[Required]
    public function setEntityManager(EntityManagerInterface $entityManager): self
    {
        $this->entityManager = $entityManager;

        return $this;
    }

    protected function configure()
    {
        $this
            ->addArgument('file-path', InputArgument::REQUIRED, 'Path of the file containing parameters')
            ->addOption('if-not-set', null, InputOption::VALUE_OPTIONAL, 'Import only if no parameters has been loaded yet', false);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $filePath = $input->getArgument('file-path');
            $ifNotSet = $input->getOption('if-not-set');

            if (!file_exists($filePath)) {
                throw new Exception('"' . $filePath . '" does not exist');
            }

            if (false !== $ifNotSet) {
                if ($this->entityManager->getRepository(Parameter::class)->count([])) {
                    $output->writeln('Parameters are already set. Skipping...');

                    return Command::SUCCESS;
                }
            }

            $parts = explode('.', $filePath);
            $extension = strtolower($parts[count($parts) - 1]);
            $parameters = match ($extension) {
                'yml', 'yaml' => Yaml::parseFile($filePath),
                'json' => json_decode(file_get_contents($filePath), true),
                default => throw new Exception('"' . $extension . '" is not a valid format. Accepted files are : JSON/YAML'),
            };

            foreach ($parameters as $name => $value) {
                $this->parameterBusiness->setValue($name, $value);
            }

            $this->entityManager->flush();

            $output->writeln('SUCCESS : Parameters imported successfully');

            return Command::SUCCESS;
        } catch (Exception $e) {
            $output->writeln('ERROR : ' . $e->getMessage());

            return Command::FAILURE;
        }
    }
}