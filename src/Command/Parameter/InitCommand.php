<?php

namespace App\Command\Parameter;

use App\Business\ParameterBusiness;
use App\Entity\Parameter;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\Service\Attribute\Required;

#[AsCommand(name: 'app:parameter:init')]
class InitCommand extends Command
{
    protected ParameterBusiness $parameterBusiness;

    protected EntityManagerInterface $entityManager;

    #[Required]
    public function setParameterBusiness(ParameterBusiness $parameterBusiness): self
    {
        $this->parameterBusiness = $parameterBusiness;

        return $this;
    }

    #[Required]
    public function setEntityManager(EntityManagerInterface $entityManager): self
    {
        $this->entityManager = $entityManager;

        return $this;
    }

    /**
     * @throws Exception
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Initializing parameters...');

        $this->parameterBusiness->setOutput($output);
        $configuredParameters = $this->parameterBusiness->getAllParameters();

        $existingParameters = $this->entityManager->getRepository(Parameter::class)->findAll();

        $names = [];
        foreach ($existingParameters as $existingParameter) {
            $names[] = $existingParameter->getName();
        }

        foreach ($configuredParameters as $parameter) {
            if (!in_array($parameter->getName(), $names)) {
                $this->entityManager->persist($parameter);
            }
        }

        $this->entityManager->flush();

        return Command::SUCCESS;
    }
}
