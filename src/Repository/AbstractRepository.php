<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;

abstract class AbstractRepository extends ServiceEntityRepository
{
    public function count(array $criteria = []): int
    {
        $main = 'main';
        $queryBuilder = $this->_em->createQueryBuilder();

        $queryBuilder
            ->select('COUNT(1)')
            ->from($this->getEntityName(), $main)
        ;

        $i = 0;
        foreach ($criteria as $key => $value) {
            $alias = 'c'.$i++;
            $expr = $queryBuilder->expr();
            $key = $main.'.'.$key;
            if (is_array($value)) {
                $queryBuilder->andWhere($expr->in($key, ':'.$alias))
                    ->setParameter($alias, $value);
            } elseif (null === $value) {
                $queryBuilder->andWhere($expr->isNull($key));
            } else {
                $queryBuilder->andWhere($expr->eq($key, ':'.$alias))
                    ->setParameter($alias, $value);
            }
        }

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    public function eagerFind(int $id, array $relationships = []): ?object
    {
        $qb = $this->createEagerFindByQueryBuilder(['id' => $id], [], $relationships);

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function eagerFindOneBy(array $criteria, array $orderBy = null, array $relationships = []): ?object
    {
        $qb = $this->createEagerFindByQueryBuilder($criteria, $orderBy, $relationships);

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function eagerFindBy(
        array $criteria,
        ?array $orderBy = null,
        ?array $relationships = [],
        ?string $indexedBy = null,
        ?int $limit = null,
        ?int $offset = null
    ): array {
        $qb = $this->createEagerFindByQueryBuilder($criteria, $orderBy, $relationships, $indexedBy, $limit, $offset);

        return $qb->getQuery()->getResult();
    }

    public function eagerFindByIndexedById(array $criteria, array $relationships = [], int $limit = null, int $offset = null): array
    {
        $qb = $this->createEagerFindByQueryBuilder($criteria, [], $relationships, 'id', $limit, $offset);

        return $qb->getQuery()->getResult();
    }

    /**
     * @throws \Exception
     */
    public function eagerFindAll(array $orderBy = null, array $relationships = []): array
    {
        $qb = $this->createEagerFindByQueryBuilder([], $orderBy, $relationships);

        return $qb->getQuery()->getResult();
    }

    public function partialFind(int $id, array $fields = []): ?object
    {
        $qb = $this->createPartialFindByQueryBuilder(['id' => $id], [], $fields);

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function partialFindOneBy(array $criteria, array $orderBy = null, array $fields = []): ?object
    {
        $qb = $this->createEagerFindByQueryBuilder($criteria, $orderBy, $fields);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @throws \Exception
     */
    public function partialFindBy(array $criteria, array $orderBy = null, array $fields = []): array
    {
        $qb = $this->createPartialFindByQueryBuilder($criteria, $orderBy, $fields);

        return $qb->getQuery()->getResult();
    }

    /**
     * @throws \Exception
     */
    public function partialFindByIndexedById(array $criteria, array $fields = []): array
    {
        if (!in_array('id', $fields, true)) {
            $fields[] = 'id';
        }
        $qb = $this->createPartialFindByQueryBuilder($criteria, [], $fields, 'id');

        return $qb->getQuery()->getResult();
    }

    /**
     * @throws \Exception
     */
    public function partialFindAll(array $orderBy = null, array $fields = []): array
    {
        $qb = $this->createPartialFindByQueryBuilder([], $orderBy, $fields);

        return $qb->getQuery()->getResult();
    }

    /**
     * @throws \ReflectionException
     */
    protected function mapOneToMany(string $fieldName, string $fromClassName, string $toClassName, string $mappedBy): void
    {
        $map = [
            'targetEntity' => $toClassName,
            'fieldName' => $fieldName,
            'mappedBy' => $mappedBy,
        ];
        $fromMetadata = $this->_em->getClassMetadata($fromClassName);
        $fromMetadata->mapOneToMany($map);
        $refl = new \ReflectionProperty($fromClassName, $fieldName);
        $refl->setAccessible(true);
        $fromMetadata->reflFields[$fieldName] = $refl;

        $toMetadata = $this->_em->getClassMetadata($toClassName);
        $toMetadata->associationMappings[$mappedBy]['inversedBy'] = $fieldName;
    }

    /**
     * @throws \ReflectionException
     */
    protected function mapManyToOne(string $fieldName, string $fromClassName, string $toClassName, string $inversedBy): void
    {
        $map = [
            'targetEntity' => $toClassName,
            'fieldName' => $fieldName,
            'inversedBy' => $inversedBy,
        ];
        $fromMetadata = $this->_em->getClassMetadata($fromClassName);
        $fromMetadata->mapManyToOne($map);
        $refl = new \ReflectionProperty($fromClassName, $fieldName);
        $refl->setAccessible(true);
        $fromMetadata->reflFields[$fieldName] = $refl;

        $toMetadata = $this->_em->getClassMetadata($toClassName);
        $toMetadata->associationMappings[$inversedBy]['mappedBy'] = $fieldName;
    }

    private function createEagerFindByQueryBuilder(
        array $criteria,
        ?array $orderBy = null,
        ?array $relationships = [],
        ?string $indexBy = null,
        ?int $limit = null,
        ?int $offset = null
    ): QueryBuilder {
        $main = 'main';
        $indexBy = null !== $indexBy ? $main.'.'.$indexBy : null;
        $qb = $this->createQueryBuilder($main, $indexBy);

        if ($this->isAssociativeArray($relationships)) {
            foreach ($relationships as $relationship => $alias) {
                if (!str_contains($relationship, '.')) {
                    $join = $main.'.'.$relationship;
                } else {
                    $join = $relationship;
                }
                $qb->leftJoin($join, $alias)
                    ->addSelect($alias);
            }
        } else {
            $i = 0;
            foreach ($relationships as $relationship) {
                $alias = 'e'.$i++;
                if (!str_contains($relationship, '.')) {
                    $join = $main.'.'.$relationship;
                } else {
                    $join = $relationship;
                }
                $qb->leftJoin($join, $alias)
                    ->addSelect($alias);
            }
        }

        $i = 0;
        foreach ($criteria as $key => $value) {
            $alias = 'c'.$i++;
            $expr = $qb->expr();
            if (!str_contains($key, '.')) {
                $key = $main.'.'.$key;
            }
            if (is_array($value)) {
                $qb->andWhere($expr->in($key, ':'.$alias))
                    ->setParameter($alias, $value);
            } elseif (null === $value) {
                $qb->andWhere($expr->isNull($key));
            } else {
                $qb->andWhere($expr->eq($key, ':'.$alias))
                    ->setParameter($alias, $value);
            }
        }

        if (null !== $orderBy) {
            foreach ($orderBy as $sort => $order) {
                if (1 == substr_count($sort, '.')) {
                    $field = $sort;
                } else {
                    $field = $main.'.'.$sort;
                }
                $qb->addOrderBy($field, $order);
            }
        }

        if (null !== $limit) {
            $qb->setMaxResults($limit);
        }

        if (null !== $offset) {
            $qb->setFirstResult($offset);
        }

        return $qb;
    }

    private function createPartialFindByQueryBuilder(
        array $criteria,
        ?array $orderBy = null,
        ?array $fields = [],
        ?string $indexBy = null
    ): QueryBuilder {
        if (0 === count($fields)) {
            throw new \Exception('Aucun attribut n\'a été défini pour la requête partielle');
        }

        $main = 'main';
        $indexBy = null !== $indexBy ? $main.'.'.$indexBy : null;
        $qb = $this->_em->createQueryBuilder();

        $qb
            ->select('partial '.$main.'.{'.implode(', ', $fields).'}')
            ->from($this->getEntityName(), $main, $indexBy)
        ;

        $i = 0;
        foreach ($criteria as $key => $value) {
            $alias = 'c'.$i++;
            $expr = $qb->expr();
            $key = $main.'.'.$key;
            if (is_array($value)) {
                $qb
                    ->andWhere($expr->in($key, ':'.$alias))
                    ->setParameter($alias, $value)
                ;
            } elseif (null === $value) {
                $qb->andWhere($expr->isNull($key));
            } else {
                $qb
                    ->andWhere($expr->eq($key, ':'.$alias))
                    ->setParameter($alias, $value)
                ;
            }
        }

        if (null !== $orderBy) {
            foreach ($orderBy as $sort => $order) {
                $field = $main.'.'.$sort;
                $qb->addOrderBy($field, $order);
            }
        }

        return $qb;
    }

    private function isAssociativeArray(array $arr): bool
    {
        if (!is_array($arr) || [] === $arr) {
            return false;
        }

        return array_keys($arr) !== range(0, count($arr) - 1);
    }
}
