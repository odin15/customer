<?php

namespace App\GraphQL\Type;

use DateTime;
use Exception;
use GraphQL\Language\AST\StringValueNode;
use GraphQL\Type\Definition\ScalarType;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;

class DateTimeType extends ScalarType implements AliasedInterface
{
    public static function getAliases(): array
    {
        return ['DateTime'];
    }

    public function serialize($value)
    {
        return $value->format('Y-m-d H:i:s');
    }

    /**
     * @throws Exception
     */
    public function parseValue($value): DateTime
    {
        return new DateTime($value);
    }

    /**
     * @param StringValueNode $valueNode
     * @throws Exception
     */
    public function parseLiteral($valueNode, ?array $variables = null): DateTime
    {
        return new DateTime($valueNode->value);
    }
}
