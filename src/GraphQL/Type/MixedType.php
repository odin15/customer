<?php

namespace App\GraphQL\Type;

use Exception;
use GraphQL\Language\AST\ValueNode;
use GraphQL\Type\Definition\ScalarType;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;

class MixedType extends ScalarType implements AliasedInterface
{
    public static function getAliases(): array
    {
        return ['Mixed'];
    }

    public function serialize($value)
    {
        return $value;
    }

    public function parseValue($value)
    {
        return $value;
    }

    /**
     * @param ValueNode $valueNode
     * @throws Exception
     */
    public function parseLiteral($valueNode, ?array $variables = null)
    {
        throw new Exception('"Mixed" value cannot be parsed as literal');
    }
}
