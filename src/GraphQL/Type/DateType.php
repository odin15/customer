<?php

namespace App\GraphQL\Type;

use DateTime;
use Exception;
use GraphQL\Language\AST\StringValueNode;
use GraphQL\Type\Definition\ScalarType;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;

class DateType extends ScalarType implements AliasedInterface
{
    public static function getAliases(): array
    {
        return ['Date'];
    }

    public function serialize($value)
    {
        return $value->format('Y-m-d');
    }

    /**
     * @throws Exception
     */
    public function parseValue($value)
    {
        return (new DateTime($value))
            ->setTime(0, 0);
    }

    /**
     * @param StringValueNode $valueNode
     * @throws Exception
     */
    public function parseLiteral($valueNode, ?array $variables = null)
    {
        return (new DateTime($valueNode->value))
            ->setTime(0, 0);
    }
}
