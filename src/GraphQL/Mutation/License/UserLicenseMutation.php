<?php

namespace App\GraphQL\Mutation\License;

use App\Entity\License;
use App\GraphQL\Mutation\AbstractMutation;
use App\Repository\LicenseRepository;
use Doctrine\DBAL\LockMode;
use Doctrine\DBAL\Exception;
use Overblog\GraphQLBundle\Error\UserError;

class UserLicenseMutation extends AbstractMutation
{
    /**
     * @throws Exception
     */
    public function incrementUserLicense(): bool
    {
        $connection = $this->em->getConnection();
        $connection->beginTransaction();
        try {
            /** @var LicenseRepository $licenseRepository */
            $licenseRepository = $this->em->getRepository(License::class);

            $license = $licenseRepository->findOneBy([
                'type' => License::TYPE_USER,
            ]);
            if (null === $license) {
                return false;
            }

            $license = $licenseRepository->find($license->getId(), LockMode::PESSIMISTIC_WRITE);

            if ($license->getUsed() < $license->getValue()) {
                $license->setUsed($license->getUsed() + 1);

                $this->em->flush();
                $connection->commit();

                return true;
            } else {
                $connection->rollBack();

                return false;
            }
        } catch (\Exception $e) {
            $connection->rollBack();
            throw new UserError($e->getMessage());
        }
    }
}
