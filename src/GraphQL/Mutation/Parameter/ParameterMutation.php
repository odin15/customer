<?php

namespace App\GraphQL\Mutation\Parameter;

use App\Business\ParameterBusiness;
use App\GraphQL\Mutation\AbstractMutation;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Contracts\Service\Attribute\Required;
use Exception;

class ParameterMutation extends AbstractMutation
{
    private ParameterBusiness $parameterBusiness;

    #[Required]
    public function setParameterBusiness(ParameterBusiness $parameterBusiness): self
    {
        $this->parameterBusiness = $parameterBusiness;

        return $this;
    }

    /**
     * @throws Exception
     */
    public function updateParameter(array $input): bool
    {
        $name = $input['name'];
        $parameter = $this->parameterBusiness->getParameter($name);

        if (!$this->parameterBusiness->isParameterWritable($name)) {
            throw new AccessDeniedException("You don't have permission to update parameter '$name'");
        }

        $value = $input['value'];
        $this->parameterBusiness->setParameterValue($parameter, $value);

        $this->em->persist($parameter);
        $this->em->flush();

        return true;
    }

    /**
     * @throws Exception
     */
    public function updateParameters(array $input): bool
    {
        try {
            $this->em->beginTransaction();
            foreach ($input as $parameter) {
                $this->updateParameter($parameter);
            }
            $this->em->commit();
        } catch (Exception $exception) {
            $this->em->rollback();
            throw $exception;
        }

        return true;
    }
}
