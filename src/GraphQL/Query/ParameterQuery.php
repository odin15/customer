<?php

namespace App\GraphQL\Query;

use App\Business\ParameterBusiness;
use App\Entity\Parameter;
use Exception;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Contracts\Service\Attribute\Required;

class ParameterQuery extends AbstractQuery
{
    protected ParameterBusiness $parameterBusiness;

    #[Required]
    public function setParameterBusiness(ParameterBusiness $parameterBusiness): self
    {
        $this->parameterBusiness = $parameterBusiness;

        return $this;
    }

    public function getClass(): string
    {
        return Parameter::class;
    }

    /**
     * @throws Exception
     */
    public function getParameter(string $name): Parameter
    {
        if (!$this->parameterBusiness->exists($name)) {
            throw new NotFoundHttpException('"'.$name.'" is not a valid parameter name');
        }

        if (!$this->parameterBusiness->isParameterReadable($name)) {
            throw new AccessDeniedException("You don't have permission to read parameter '$name'");
        }

        $parameter = $this->parameterBusiness->getParameter($name);
        $parameter->setValue($this->parameterBusiness->getParameterValue($parameter));

        return $parameter;
    }

    /**
     * @throws Exception
     */
    public function getParameters(): array
    {
        $parameters = [];
        foreach ($this->parameterBusiness->getAllParameters() as $parameter) {
            try {
                $parameters[] = $this->getParameter($parameter->getName());
            } catch (Exception $exception) {
                continue;
            }
        }

        return $parameters;
    }

    public function getValueByName(string $name): ?string
    {
        if (!$this->parameterBusiness->exists($name)) {
            throw new NotFoundHttpException('"'.$name.'" is not a valid parameter name');
        }

        $parameter = $this->getRepository()->findOneBy(['name' => $name]);

        return $parameter->getValue();
    }

    /**
     * @throws Exception
     */
    public function getValues(): array
    {
        $arr = [];
        $parameters = $this->parameterBusiness->getAllParameters();
        foreach ($parameters as $parameter) {
            $arr[$parameter->getName()] = $parameter->getValue();
        }

        return $arr;
    }

    /**
     * @throws Exception
     */
    public function getParameterConfiguration(): iterable
    {
        return $this->parameterBusiness->getAllParameters();
    }
}
