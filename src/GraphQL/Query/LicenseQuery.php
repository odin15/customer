<?php


namespace App\GraphQL\Query;

use App\Entity\License;

class LicenseQuery extends AbstractQuery
{
    public function getClass(): string
    {
        return License::class;
    }

    public function findOneByType(string $type): ?License
    {
        return $this->getRepository()->findOneBy(['type' => $type]);
    }
}
