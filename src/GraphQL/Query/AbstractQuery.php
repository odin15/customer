<?php

namespace App\GraphQL\Query;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use GraphQL\Type\Definition\ResolveInfo;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Definition\Resolver\QueryInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Contracts\Service\Attribute\Required;

abstract class AbstractQuery implements QueryInterface
{
    protected EntityManagerInterface $em;

    abstract protected function getClass(): string;

    #[Required]
    public function setEntityManager(EntityManagerInterface $em): void
    {
        $this->em = $em;
    }

    public function getRepository(?string $class = null): ObjectRepository
    {
        return $this->em->getRepository(null === $class ? $this->getClass() : $class);
    }

    public function __invoke(ResolveInfo $info, $value, Argument $args)
    {
        if (method_exists($this, $info->fieldName)) {
            $method = $info->fieldName;

            return $this->$method($value, $args);
        }

        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        if ($propertyAccessor->isReadable($value, $info->fieldName)) {
            return $propertyAccessor->getValue($value, $info->fieldName);
        }

        throw new \Exception('No getter found for field "'.$info->fieldName.'"');
    }

    public function find(int $id)
    {
        return $this->em->find($this->getClass(), $id);
    }

    public function findBy(Argument $args): array
    {
        return $this->getRepository()->findBy($args->getArrayCopy());
    }

    public function findAll(): array
    {
        return $this->getRepository()->findAll();
    }
}
