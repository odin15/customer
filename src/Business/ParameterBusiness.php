<?php

namespace App\Business;

use App\Entity\Parameter;
use App\Repository\ParameterRepository;
use Doctrine\ORM\EntityManagerInterface;
use Odevia\MicroserviceServerBundle\Security\Security;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;

class ParameterBusiness
{
    protected OutputInterface $output;
    protected ?array $parameters = null;

    /** @var mixed[] */
    protected array $values = [];

    public function __construct(
        private EntityManagerInterface $em,
        private ParameterRepository $parameterRepository,
        private string $parametersFilePath,
        private Security $security
    ) {
        $this->output = new NullOutput();
    }

    public function setOutput(OutputInterface $output): self
    {
        $this->output = $output;

        return $this;
    }

    public function exists(string $name): bool
    {
        $this->loadParameters();

        return isset($this->parameters[$name]);
    }

    /**
     * @throws \Exception
     */
    public function getValue(string $name): mixed
    {
        if (!isset($this->values[$name])) {
            $parameter = $this->getParameter($name);
            $this->fillParameter($parameter);

            $this->values[$name] = $this->getParameterValue($parameter);
        }

        return $this->values[$name];
    }

    /**
     * @throws \Exception
     */
    public function setValue(string $name, mixed $value, bool $persist = true): self
    {
        $parameter = $this->getParameter($name);
        $this->setParameterValue($parameter, $value);

        if ($persist) {
            $this->em->persist($parameter);
        }

        if (isset($this->values[$name])) {
            $this->values[$name] = $parameter->getValue();
        }

        return $this;
    }

    /**
     * @throws \Exception
     */
    public function getParameterValue(Parameter $parameter): mixed
    {
        switch ($parameter->getType()) {
            case 'string':
                return $parameter->getValue();
            case 'int':
                return (int) $parameter->getValue();
            case 'float':
                return (float) $parameter->getValue();
            case 'bool':
                return (bool) $parameter->getValue();
            case 'date':
            case 'datetime':
                return new \DateTime($parameter->getValue());
            case 'json':
                return json_decode($parameter->getValue(), true);
            default:
                throw new \Exception('Unhandled "'.$parameter->getType().'" parameter type');
        }
    }

    public function setParameterValue(Parameter $parameter, mixed $value): void
    {
        switch ($parameter->getType()) {
            case 'date':
                /* @var \DateTimeInterface $value */
                $parameter->setValue($value->format('Y-m-d'));
                break;
            case 'datetime':
                /* @var \DateTimeInterface $value */
                $parameter->setValue($value->format(\DateTimeInterface::ATOM));
                break;
            case 'json':
                $parameter->setValue(json_encode($value));
                break;
            default:
                $parameter->setValue((string) $value);
                break;
        }
    }

    /**
     * @throws \Exception
     */
    public function getParameter(string $name): Parameter
    {
        $parameter = $this->parameterRepository->findOneByName($name);

        if (null === $parameter) {
            $parameter = $this->createParameter($name);
        } else {
            $this->fillParameter($parameter);
        }

        return $parameter;
    }

    /**
     * @return Parameter[]
     */
    public function getAllParameters(): array
    {
        $values = [];

        $parameters = $this->parameterRepository->findAll();
        foreach ($parameters as $parameter) {
            $values[$parameter->getName()] = $parameter;
        }

        $this->loadParameters();
        foreach ($this->parameters as $name => $parameter) {
            if (!isset($values[$name])) {
                $values[$name] = $this->createParameter($name);
            } else {
                $this->fillParameter($values[$name]);
            }
        }

        return array_values($values);
    }

    /**
     * @throws \Exception
     */
    protected function fillParameter(Parameter $parameter): void
    {
        $this->loadParameters();

        if (!isset($this->parameters[$parameter->getName()])) {
            throw new \Exception('"'.$parameter->getName().'" is not a valid parameter name');
        }

        $config = $this->parameters[$parameter->getName()];

        $parameter
            ->setType($config['type'] ?? 'string')
            ->setNullable((bool) ($config['nullable'] ?? true))
            ->setDefault($config['default'] ?? null)
            ->setPublic($config['public'] ?? false)
        ;
    }

    protected function loadParameters(): void
    {
        if (null === $this->parameters) {
            $parameters = Yaml::parseFile($this->parametersFilePath)['parameters'];
            $this->parameters = [];
            foreach ($parameters as $parameter) {
                $this->parameters[$parameter['name']] = $parameter;
            }
        }
    }

    protected function createParameter($name): Parameter
    {
        $parameter = (new Parameter())
            ->setName($name);
        $this->fillParameter($parameter);

        $type = $parameter->getType();
        $value = $environmentValue = $this->getEnvironmentValue($name);

        try {
            if (null === $value) {
                $value = $parameter->getDefault();
            } else {
                $value = $this->checkValue($type, $value);
            }
        } catch (\Exception $e) {
            $this->output->writeln("{$parameter->getName()}: Mal formatted value \"$environmentValue\" for type \"{$type}\"");
            $value = $parameter->getDefault();
        }

        $this->setParameterValue($parameter, $value);

        return $parameter;
    }

    /**
     * @throws \Exception
     */
    protected function checkValue(string $type, mixed $value): mixed
    {
        switch ($type) {
            case 'int':
                if ($value != (int) ($value)) {
                    throw new \Exception();
                }

                return (int) ($value);
            case 'bool':
                if ('false' === $value || '0' === $value || false === $value || 0 === $value) {
                    return '0';
                } elseif ('true' === $value || '1' === $value || true === $value || 1 === $value) {
                    return '1';
                } else {
                    throw new \Exception();
                }
            // no break
            case 'float':
                if ($value != (float) ($value)) {
                    throw new \Exception();
                }

                return (float) ($value);
            case 'string':
                return strval($value);
            case 'date':
                if (false === \DateTime::createFromFormat('Y-m-d', $value)) {
                    throw new \Exception();
                }

                return $value;
            case 'datetime':
                if (false === \DateTime::createFromFormat('Y-m-d H:i:s', $value)) {
                    throw new \Exception();
                }

                return $value;
            case 'json':
                return json_decode($value, true);
            default:
                throw new \Exception("Unhandled \"$type\" parameter type");
        }
    }

    protected function getEnvironmentValue(string $name): ?string
    {
        return $_SERVER[$name] ?? null;
    }

    public function isParameterReadable(string $name): bool
    {
        return $this->getParameter($name)->isPublic() || $this->security->isUserAuthenticated() || $this->security->isServiceAuthenticated();
    }

    public function isParameterWritable(string $name): bool
    {
        return $this->security->isUserAuthenticated() || $this->security->isServiceAuthenticated();
    }
}
