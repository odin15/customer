<?php

namespace App\DataFixtures;

use App\Entity\License;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class LicenseFixtures extends Fixture
{
    public const REFERENCE_LICENSE_USER = 'REFERENCE_LICENSE_USER';

    public function load(ObjectManager $manager)
    {
        // User license
        $userLicense = (new License())
            ->setType(License::TYPE_USER)
            ->setValue(5)
            ->setUsed(2)
        ;
        $this->addReference(self::REFERENCE_LICENSE_USER, $userLicense);
        $manager->persist($userLicense);

        $manager->flush();
    }
}
