<?php

namespace App\DataFixtures;

use App\Entity\Parameter;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ParameterFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $manager->persist((new Parameter())
            ->setName('CUSTOMER_NAME')
            ->setType('string')
            ->setValue('Dyosis')
            ->setNullable(false)
        );

        $manager->flush();
    }
}
